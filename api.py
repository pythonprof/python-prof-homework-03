#!/usr/bin/env python3
"""
Very simple HTTP server in python for logging requests
Usage::
    ./server.py [<port>]
"""
import datetime
import hashlib
import json
import logging
import scoring
import uuid
from fields import CharField, ArgumentsField, PhoneField, ClientIDsField, EmailField
from fields import DateField, BirthDayField, BaseField, GenderField
from fields import RequestError
from fields import OK, BAD_REQUEST, FORBIDDEN, NOT_FOUND, INVALID_REQUEST, INTERNAL_ERROR
from http.server import BaseHTTPRequestHandler, HTTPServer
from optparse import OptionParser

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"

ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


def check_auth(request):
    if request.is_admin:
        digest = hashlib.sha512((datetime.datetime.now().strftime("%Y%m%d%H") + ADMIN_SALT).encode('utf-8')).hexdigest()
    else:
        digest = hashlib.sha512((request.account.value + request.login.value + SALT).encode('utf-8')).hexdigest()
    if digest == request.token.value:
        return True
    return False


class BaseRequest:
    def __init__(self, request):
        self.request = request

    def prepare_data(self, acontext):
        pass

    def validate(self):
        for field_name in self.__dict__:
            field = self.__dict__[field_name]
            if isinstance(field, BaseField):
                field.validate()
        if "arguments" not in self.request["body"]:
            raise RequestError("No arguments in request data", INVALID_REQUEST)
        if "method" not in self.request["body"]:
            raise RequestError("No method in request data", INVALID_REQUEST)

    # Будет переопределен в потомках
    def create_response_data(self):
        return {"test_parent_data": "Hello, world !!!"}

    def get_response(self, ctx, store):
        try:
            if "arguments" not in self.request["body"]:
                raise RequestError("No arguments in request", INVALID_REQUEST)

            if self.token.value in [None, ""]:
                raise RequestError("Empty token", FORBIDDEN)

            if self.login.value is None:
                raise RequestError("Empty login", INVALID_REQUEST)

            if not check_auth(self):
                raise RequestError("Forbidden", FORBIDDEN)

            ctx["has"] = self.request["body"]["arguments"].keys()
            self.prepare_data(ctx)

            self.validate()

            response = self.create_response_data()

            return response, 200
        except RequestError as e:
            logging.exception("Request error: %s" % e)
            return e.message, e.code

        except Exception as e:
            logging.exception("Internal error: %s" % e)
            return str(e), FORBIDDEN


class MethodRequest(BaseRequest):
    def __init__(self, request):
        super(MethodRequest, self).__init__(request)
        self.account = CharField(required=False, nullable=True, request=request, name="account")
        self.login = CharField(required=True, nullable=True, request=request, name="login")
        self.token = CharField(required=True, nullable=True, request=request, name="token")
        self.arguments = ArgumentsField(required=True, nullable=True, request=request, name="arguments")
        self.method = CharField(required=True, nullable=False, request=request, name="method")

    @property
    def is_admin(self):
        return self.login.value == ADMIN_LOGIN


class OnlineScoreRequest(MethodRequest):
    def prepare_data(self, acontext):
        self.phone = PhoneField(required=False, nullable=True,
                                request=self.arguments.value, name="phone")
        self.email = EmailField(required=False, nullable=True,
                                request=self.arguments.value, name="email")
        self.first_name = CharField(required=False, nullable=True,
                                    request=self.arguments.value, name="first_name")
        self.last_name = CharField(required=False, nullable=True,
                                   request=self.arguments.value, name="last_name")
        self.birthday = BirthDayField(required=False, nullable=True,
                                      request=self.arguments.value, name="birthday")
        self.gender = GenderField(required=False, nullable=True,
                                  request=self.arguments.value, name="gender")

    def validate(self):
        super(OnlineScoreRequest, self).validate()
        empty_couple1 = (self.phone.value is None) or (self.email.value is None)
        empty_couple2 = (self.first_name.value is None) or (self.last_name.value is None)
        empty_couple3 = (self.gender.value is None) or (self.birthday.value is None)
        if empty_couple1 and empty_couple2 and empty_couple3:
            raise RequestError('No not an empty pair of conditions', INVALID_REQUEST)

    def create_response_data(self):
        if self.is_admin:
            return {'score': 42}
        else:
            score = scoring.get_score(store=None, phone=self.phone.value, email=self.email.value,
                                      birthday=self.birthday.value, gender=self.gender.value,
                                      first_name=self.first_name.value, last_name=self.last_name.value)
            return {'score': score}


class ClientsInterestsRequest(MethodRequest):
    def prepare_data(self, acontext):
        self.client_ids = ClientIDsField(required=True,
                                         request=self.arguments.value, name="client_ids")
        self.date = DateField(required=False, nullable=True,
                              request=self.arguments.value, name="date")
        if self.client_ids.value is not None:
            acontext["nclients"] = len(self.client_ids.value)
        else:
            acontext["nclients"] = 0

    def create_response_data(self):
        response = {cid: scoring.get_interests(None, cid) for cid in self.client_ids.value}
        return response


def online_score_handler(request, ctx, store):
    return OnlineScoreRequest(request).get_response(ctx, store)


def clients_interests_handler(request, ctx, store):
    return ClientsInterestsRequest(request).get_response(ctx, store)


def method_handler(request, ctx, store):
    method_class_handlers = {
        'clients_interests': ClientsInterestsRequest,
        'online_score': OnlineScoreRequest
    }
    if "body" not in request:
        return "Not found body in request", BAD_REQUEST
    if "method" not in request["body"]:
        return "Not found method in request", BAD_REQUEST
    method = request["body"]["method"]
    if method not in method_class_handlers:
        return "Invalid method in request", BAD_REQUEST
    return method_class_handlers[method](request).get_response(ctx, store)


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {"method": method_handler}
    store = None

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except Exception:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info("%s: %s %s" % (self.path, data_string, context["request_id"]))
            if path in self.router:
                try:
                    code = 200
                    response, code = self.router[path]({"body": request, "headers": self.headers}, context, self.store)
                except Exception as e:
                    logging.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(code, "Unknown Error"), "code": code}
        context.update(r)
        logging.info(context)

        self.wfile.write(json.dumps(r, ensure_ascii=False).encode('utf-8'))


if __name__ == '__main__':
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
