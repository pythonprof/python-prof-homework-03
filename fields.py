import datetime

OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500


class RequestError(Exception):
    def __init__(self, message, code):
        self.code = code
        self.message = message
        super().__init__(self.message)


class BaseField:
    def __init__(self, required=False, nullable=False, value=None, request=None, name=None):
        """ required bool   - обязательно ли значение поле
            nullable bool   - может ли значение поля равняться None
            value           - значение поля. В зависимости от класса поля тип значения может различаться
            request         - словарь возможных значений. если не None и name входит в словарь,
                              то устанавливаем self.value в значение name из словаря
            name            - имя атрибута словаря, который устанавливаем в self.value
        """
        self.required = required
        self.nullable = nullable
        self.field_name = name
        self.__value = None
        self.value = value

        if isinstance(request, dict):
            if 'body' in request:
                data_dict = request['body']
            else:
                data_dict = request

            if isinstance(data_dict, dict):
                if name in data_dict:
                    self.value = data_dict[name]

    def prepare_value(self, val):
        return val

    @property
    def value(self):
        return self.__value

    @value.setter
    def value(self, val):
        self.__value = self.prepare_value(val)

    def validate(self):
        if self.value is None and self.required:
            raise RequestError('Field "' + str(self.field_name) + '" may not be empty', INVALID_REQUEST)


class GenderField(BaseField):

    allowed_gender_values = {0, 1, 2, None}

    def validate(self):
        super(GenderField, self).validate()
        if self.value not in self.allowed_gender_values:
            raise RequestError('Field Gender must be 0, 1 or 2', INVALID_REQUEST)


class CharField(BaseField):
    def validate(self):
        super(CharField, self).validate()
        if not (self.value is None or isinstance(self.value, str)):
            raise RequestError('Type of field "' + str(self.field_name) + '" must string or empty', INVALID_REQUEST)


class EmailField(CharField):
    def is_valid_email(self):
        if self.value is None:
            return True
        if not (self.value.count("@") == 1):
            return False
        dog_pos = self.value.find("@")
        if dog_pos <= 0:
            return False
        if dog_pos >= len(self.value) - 1:
            return False
        return True

    def validate(self):
        super(EmailField, self).validate()
        if not self.is_valid_email():
            raise RequestError('Field "' + str(self.field_name) + '" not correct email', INVALID_REQUEST)


class PhoneField(CharField):
    def prepare_value(self, val):
        if val is None:
            return
        if isinstance(val, str):
            return val
        if isinstance(val, int):
            return str(val)
        return val

    def validate(self):
        super(PhoneField, self).validate()
        if self.value is None:
            return
        if not ((len(self.value) == 11) and (self.value[0] == "7")):
            raise RequestError('Field "' + str(self.field_name) + '" not correct phone', INVALID_REQUEST)


class DateField(CharField):
    def get_date(self):
        try:
            d = datetime.datetime.strptime(self.value, "%d.%m.%Y")
            d = datetime.date(d.year, d.month, d.day)
            return d
        except Exception:
            return

    def validate(self):
        super(DateField, self).validate()

        if self.value is None:
            return

        if self.get_date() is None:
            raise RequestError('Field "' + str(self.field_name) + '" must be in DD.MM.YYYY format', INVALID_REQUEST)


class BirthDayField(DateField):
    def validate(self):
        super(BirthDayField, self).validate()
        if self.value is None:
            return
        d = datetime.date.today()
        last70 = datetime.date(d.year - 70, d.month, d.day)
        if self.get_date() < last70:
            raise RequestError('Field "' + str(self.field_name) + '" is too early', INVALID_REQUEST)


class ArgumentsField(BaseField):
    def validate(self):
        super(ArgumentsField, self).validate()
        if type(self.value) is not dict:
            raise RequestError('Field "' + str(self.field_name) + '" is not dict', INVALID_REQUEST)


class ClientIDsField(BaseField):
    def validate(self):
        super(ClientIDsField, self).validate()
        if not isinstance(self.value, list):
            raise RequestError('Field "' + str(self.field_name) + '" is not list', INVALID_REQUEST)
        if not len(self.value):
            raise RequestError('Field "' + str(self.field_name) + '" is empty', INVALID_REQUEST)
        for _id in self.value:
            if not isinstance(_id, int):
                raise RequestError("client_ids must consist of int types", INVALID_REQUEST)
