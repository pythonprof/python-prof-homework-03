import requests
import json

BAD_GET_INTERESTS_REQUEST_DATA = {
        "account": "horns&hoofs",
        "login": "h&f",
        "method": "clients_interests",
        "arguments": {'client_ids': [], 'date': '20.07.2017'}
	   }


BAD_AUTH_REQUEST_DATA = {
        "account": "horns&hoofs",
        "login": "h&f",
        "method": "online_score",
        "token": "",
        "arguments": {}
	   }

CORRECT_ONLINE_SCORE_REQUEST_DATA = {
        "account": "horns&hoofs",
        "login": "h&f",
        "method": "online_score",
        "token": "55cc9ce545bcd144300fe9efc28e65d415b923ebb6be1e19d2750a2c03e80dd209a27954dca045e5bb12418e7d89b6d718a9e35af34e14e1d5bcd5a08f21fc95",
        "arguments": {"phone":"79175002040", "email": "stupnikov@otus.ru", "first_name": "Станислав", "gender": 1, "birthday": "01.01.2020"}
	   }

CORRECT_GET_INTERESTS_REQUEST_DATA = {
        "account": "horns&hoofs",
        "login": "h&f",
        "method": "clients_interests",
        "token": "55cc9ce545bcd144300fe9efc28e65d415b923ebb6be1e19d2750a2c03e80dd209a27954dca045e5bb12418e7d89b6d718a9e35af34e14e1d5bcd5a08f21fc95",
        "arguments": {"client_ids":[1,2,3,4], "date": "20.07.2017"}
	   }

def DoRequest(request_data):
    addr = "http://127.0.0.1:8080/method"
    # addr = "http://127.0.0.1:8080/" + request_data["method"]
    resp = requests.post(addr, json.dumps(request_data, ensure_ascii=False).encode())
    print(resp.content.decode())


DoRequest(CORRECT_ONLINE_SCORE_REQUEST_DATA)
DoRequest(CORRECT_GET_INTERESTS_REQUEST_DATA)
DoRequest(BAD_AUTH_REQUEST_DATA)
DoRequest(BAD_GET_INTERESTS_REQUEST_DATA)
